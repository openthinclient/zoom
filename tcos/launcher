#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: set filetype=python noexpandtab:

from __future__ import division, unicode_literals
import logging
import os
import signal
import sys
import subprocess
import tcos_system

if __name__ == '__main__':
    # Working interruption by Ctrl-C
    signal.signal(signal.SIGINT, signal.default_int_handler)
    # Sets utf-8 (instead of latin1) as default encoding for every IO
    reload(sys); sys.setdefaultencoding('utf-8')
    # Run in application's working directory
    os.chdir((os.path.dirname(os.path.realpath(__file__)) or '.') + '/.'); sys.path.insert(0, os.path.realpath(os.getcwd()))

# Configures logging
try:
    import tcos_logging
    tcos_logging.init(logger=logging.getLogger(__name__), default_level='WARNING')
except ImportError:
    # Attention: does not redirect stderr
    logging.basicConfig(level=logging.WARNING, datefmt='%H:%M:%S', format='%(asctime)s.%(msecs)03d %(pathname)s:%(lineno)d [%(levelname)s]  %(message)s')

# Configures exception hook
try:
    import tcos_exception_hook
    tcos_exception_hook.init()
except ImportError:
    pass

zoom_config_path = '/home/tcos/.config/zoomus.conf'

def _write_config():
    if not os.path.exists(zoom_config_path):
        available_languages = ('en', 'de', 'es', 'fr', 'it', 'jp', 'ko', 'ru', 'vi', 'zh')
        system_language = os.getenv('LANG')[0:2]
        zoom_language = system_language if system_language in available_languages else 'en'
        with open(zoom_config_path, 'w') as config_file:
            config_file.write('[General]\nlanguage={}\n'.format(zoom_language))


def run_init():
    """Default entry point

    """
    _write_config()

    # Kill all zoom instances
    try:
        logging.getLogger(__name__).info('Killing previous zoom instances.')
        tcos_system.run_command('killall -9 /opt/zoom/zoom', raise_exceptions=True)
    except subprocess.CalledProcessError:
        logging.getLogger(__name__).info('Could not kill zoom. Maybe it was not running.')

    logging.getLogger(__name__).info('Starting Zoom.')
    tcos_system.run_command('/usr/bin/zoom')


def run_setup():
    """Entry point to setup zoom-related mime types"""

    mime_types = [
        'application/x-zoom'
        'x-scheme-handler/callto',
        'x-scheme-handler/tel',
        'x-scheme-handler/zoommtg',
        'x-scheme-handler/zoomphonecall',
        'x-scheme-handler/zoomus',
    ]

    for mime_type in mime_types:
        tcos_system.set_default_application(mime_type, 'Zoom.desktop')

def _main():
    import argparse
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS, add_help=False)
    parser.add_argument('-r', '--run-function', default='init', choices=[k[len('run_'):] for k in globals() if k.startswith('run_')], help='Function to run (without "run_"-prefix)')
    parser.add_argument('-v', '--verbose', action='count', help='Raises logging level')
    kwargs = vars(parser.parse_known_args()[0])  # Breaks here if something goes wrong

    # Raises verbosity level for script (through arguments -v and -vv)
    logging.getLogger(__name__).setLevel((logging.WARNING, logging.INFO, logging.DEBUG)[min(kwargs.get('verbose', 0), 2)])

    globals()['run_' + kwargs['run_function']]()


if __name__ == '__main__':
    _main()
